from flask import Flask, jsonify
from . import data
import click

app = Flask(__name__)


# REST API Endpoints
@app.route("/personal", methods=["GET"])
def personal():
    return jsonify(data.personal_data)


@app.route("/experience", methods=["GET"])
def experience():
    return jsonify(data.experience_data)


@app.route("/education", methods=["GET"])
def education():
    return jsonify(data.education_data)


# Basic error handling
@app.errorhandler(404)
def not_found(error):
    return jsonify({'error': 'Not found'}), 404


# CLI command
@app.cli.command("print-cv-data")
def print_cv_data():
    print("Personal Data:")
    print(data.personal_data)
    print("Experience Data:")
    print(data.experience_data)
    print("Education Data:")
    print(data.education_data)


if __name__ == '__main__':
    app.run()
