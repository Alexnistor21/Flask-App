# CV data
personal_data = {
        "name": "Alexandru Nistor",
        "email": "alexnistorr21@gmail.com",
        "phone": "0739596006"
        }

experience_data = [
        {
            "company": "Software Mirage",
            "title": "Software Engineer",
            "duration": "January 2020 - February 2021",
            "description": "Worked on a proof of concept for an e-book selling platform where I was involved in bug "
                           "fixing, adding new features, and developing backend services related to payments (payment "
                           "provider integration - PayPal, paymill) and fraud analysis (Chase Paymentech Safetech "
                           "Fraud and Security)"
        },
        {
            "company": "Company 2",
            "title": "Software Engineer",
            "duration": "February 2021 - April 2022",
            "description": "Worked on an invoice management project where I was responsible for the creation of "
                           "Rest API’s that handle generating, viewing, and editing invoices and user accounts, "
                           "as well as writing unit tests for them. I was also involved in performing a Python2.7 "
                           "to Python3 upgrade for various web services in the code."
        },
        {
            "company": "Capgemini Engineering",
            "title": "Software Engineer",
            "duration": "Currently not involved in any commercial project, I've been on the bench for the past "
                        "couple of months, increasing my skills and technology stack with Django, Docker, Poetry,"
                        " SQLAlchemy."
        }
    ]

education_data = {
        "degree": "Python course",
        "university": "Software Development Academy",
        "duration": "May 2019-December 2019",
        }
